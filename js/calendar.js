const days = ["mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag", "søndag"];
const daysShort = ["man", "tirs", "ons", "tors", "fre", "lør", "søn"];
const months = ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"];
const monthsCapital = ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"];

const today = new Date();
var selectedDay;
var offsetMonths = 0;
let currentMonthIndex;

const weekRows = 6;

var calendarDiv = document.getElementById("calendarContainter");

calendarDiv.innerHTML += `<div id="topRow"></div>`; // creating top header row
var topRow = document.getElementById("topRow");

topRow.innerHTML += `<button onclick="drawOtherMonth(-1)" class="changeMonthBtn">◄</button> 
					<div id="monthHeader"></div>
					<button onclick="drawOtherMonth(+1)" class="changeMonthBtn">►</button>`; // creating buttons and header inside top row

calendarDiv.innerHTML += `<div id="daysRow" class="row"></div>`;

var dayNamesDiv = document.getElementById("daysRow");
for (day of daysShort)
	dayNamesDiv.innerHTML += `<div class="rowBox">`+day+`</div>`;

var baseCalendar = calendarDiv.innerHTML;
var openDate = null;

drawMonth();

var plainLoadedCalendar = calendarDiv.innerHTML;

var canOpenHours = true;

function initDaySelect ()
{
	canOpenHours = false;
}

function initHourSelect()
{
	canOpenHours = true;
}

function drawMonth ()
{
	calendarDiv.innerHTML = baseCalendar;

	let startDate = new Date(today.getFullYear(), today.getMonth() + offsetMonths); // setting top-left day
	currentMonthIndex = startDate.getMonth();
	let startDateInt = startDate.getDate() - startDate.getDay() - 6;
	if (startDate.getDay() != 0) // if monday = 1st
		startDateInt += 7;

	startDate.setDate(startDateInt);

	let queuedDate = startDate;

	document.getElementById("monthHeader").innerHTML = monthsCapital[currentMonthIndex] + " " + queuedDate.getFullYear();

	for (let w = 0; w < weekRows; w++)
	{
		calendarDiv.innerHTML += `<div id="week_` + w + `" class="row"></div>`;
		let weekRowDiv = document.getElementById("week_" + w);
		for (let d = 0; d < 7; d++)
		{
		//	today-style
			var todayStyle = "";
			if (equalDates(queuedDate, today))
				todayStyle = ` today `;

		//	other month-style
			var otherMonthStyle = "";
			if (queuedDate.getMonth() != currentMonthIndex)
				otherMonthStyle = ` otherMonth `;

		//	make day-div
			let _year = queuedDate.getFullYear();
			let _month = queuedDate.getMonth();
			let _date = queuedDate.getDate();

			let dateId = _year+"_"+_month+"_"+_date;
			weekRowDiv.innerHTML += `<div class="rowBox day`+otherMonthStyle+todayStyle+`" onclick="clickDate(`+_year + ", " + _month + ", " + _date + ", " + w +`)" id="` + dateId + `">`
			+ queuedDate.getDate() + `</div >`;

			queuedDate.setDate(queuedDate.getDate() + 1);
		}
	}

	plainLoadedCalendar = calendarDiv.innerHTML;
}

function selectDay (year, month, date)
{
	console.log("selectDay()");
	var deseleced = false;

	if (selectedDay != null)
		document.getElementById(selectedDay.getFullYear()+"_"+selectedDay.getMonth()+"_"+selectedDay.getDate()).className = 
		document.getElementById(selectedDay.getFullYear()+"_"+selectedDay.getMonth()+"_"+selectedDay.getDate()).className.replace("focusDay", "");

	if (document.getElementById(year+"_"+month+"_"+date).className.search("focusDay") == -1)
		document.getElementById(year+"_"+month+"_"+date).className += " focusDay";

	if (equalDates(selectedDay, new Date(year, month, date)))
	{
		document.getElementById(selectedDay.getFullYear()+"_"+selectedDay.getMonth()+"_"+selectedDay.getDate()).className = 
		document.getElementById(selectedDay.getFullYear()+"_"+selectedDay.getMonth()+"_"+selectedDay.getDate()).className.replace("focusDay", "");
		deseleced = true;
	}

	if (deseleced)
		selectedDay = null;
	if (!deseleced)
		selectedDay = new Date(year, month, date);
}

function clickDate (year, month, date, weekRow)
{
	if (!canOpenHours)
	{
		selectDay(year, month, date);
		return;
	}

	calendarDiv.innerHTML = plainLoadedCalendar;
	let inputDate = new Date(year, month, date);
	selectedDay = inputDate;

	if (equalDates(inputDate, openDate))
	{
		openDate = null;
		return;
	}

	var aboveRows = "";
	var belowRows = "";

	for (let row = 2; row < 8; row++)
	{
		if (row < weekRow + 3)
			aboveRows += calendarDiv.childNodes[row].outerHTML;

		else if (calendarDiv.childNodes[row] != null)
			belowRows += calendarDiv.childNodes[row].outerHTML;
	}

	var mayHours = getMayHours();

	var hourRow = `<div class="row hourRow">`;
	for (let d = 0; d < 7; d++)
	{
		hourRow += `<div class="hourRowDays">`;

		for (let i = 0; i < 24; i++)
		{
			let taken = "";
			if (year == 2018 && month == 4)
			{
				if (!mayHours[d][i])
				taken = " hourTaken";
			}
			hourRow += `<div class="hourBox`+taken+`" id="`+year+"_"+month+"_"+date+"_"+i+`" onclick="hourClick(`+year+", "+month+", "+date+", "+i+`)">`+i+`</div>`;
		}

		hourRow += `</div>`
	}
	hourRow += `</div>`;

	calendarDiv.innerHTML = baseCalendar + aboveRows + hourRow + belowRows;
	document.getElementById(year+"_"+month+"_"+date).className += " focusDay ";
	document.getElementById("monthHeader").innerHTML =  monthsCapital[currentMonthIndex] + " " + year;

	openDate = inputDate;

	var mayHours = getMayHours();
}

function hourClick(year, month, date, hour)
{
	console.log("clicked on:",year, month, date, hour);

	var mayHours = getMayHours();

	if ((year == 2018 && month == 4) || mayHours[date] == null) return;


	if (mayHours[date][hour])
	{
		var hourElement = document.getElementById(year+"_"+month+"_"+date+"_"+hour);
		if (hourElement.className.search("hourSelected") != -1)
			hourElement.className = hourElement.className.replace("hourSelected", "");

		else
		{
			hourElement.className += " hourSelected";
		}
	}

	if (!mayHours[date][hour])
		console.log ("hour is already booked");
}

function getMayHours ()
{
	let jsonObj = JSON.parse(dummyBookings);

	var reservedBools = [];

	for (let d = 0; d < jsonObj.thisYear.mai.length; d++)
	{
		reservedBools.push([]);
		for (let h = 0; h < jsonObj.thisYear.mai.length; h++)
		{
			let hBool = false;
			if (jsonObj.thisYear.mai[d][h] == "1")
				hBool = true;
			reservedBools[d].push(hBool);
		}
	}

	return reservedBools;
}

function drawOtherMonth (offset)
{
	selectedDay = null;
	offsetMonths += offset;
	drawMonth();
}

function equalDates (date1, date2)
{
	if (typeof(date1) != "object" || typeof(date2) != "object" || date1 == null || date2 == null)
		return false;

	return (date1.getDate() == date2.getDate() && date1.getMonth() == date2.getMonth() && date1.getFullYear() == date2.getFullYear());
}

function print (msg){console.log(msg);}

var dummyBookings = `
{
	"thisYear": {
		"mai": [
			"000111111111111000000000",
			"001100000000000001111100",
			"000000111111100000000000",
			"000000000000000000000000",
			"000111111111111000000000",
			"000000000000000001111100",
			"000000111111100000000000",
			"001100000000000000011000",
			"000000000000000000000000",
			"000111111111111000000000",
			"000000000000000001111100",
			"000000111111100000000000",
			"000000000000111100000000",
			"000011100000000000000000",
			"000111111111111000000000",
			"000000000000000001111100",
			"111111111111111111111111",
			"000001111100000000111100"
		]
	}
}`;